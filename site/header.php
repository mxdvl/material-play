<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="<?php echo cacheBust('css/normalize.css'); ?>">
        <link rel="stylesheet" href="<?php echo cacheBust('css/main.css'); ?>">
        <script src="js/vendor/modernizr-2.6.1.min.js"></script>

        <style type="text/css">
        .box.full svg path.hover,
        .box.full svg path._<?php echo $project[0]; ?> {
            opacity: 1;
        }
        </style>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->

        <header id="<?php echo $project[0]; ?>" class="clearfix">
            <h1><a href="./">ARCH 572 — Material Play</a></h1>

            <a class="zero-box" href="./"></a>

        <?php

/**********************************************************************

    STEP 1

**********************************************************************/

        for ($i=0; $i < 38; $i++) {
            if($i < 9)
                 $k = "0".($i+1);
            else $k =     ($i+1);

            if($k == $project[0]) {
                $current = "current";
                $box = "div";
            } else {
                $box = 'a  href="'.($k).'-'.toAscii($names[$i+1]).'"';
                $current = "";
            }

            echo '<'.$box.' class="box one '.$current.' " '.randomColor(($i)*0-1).'>';
            if(is_dir('content/'.($k))) echo '<img src="content/'.($k).'/th.jpg">';
            echo '</'.trim(substr($box, 0, 3)).'>';

            echo '

            ';
        }

        echo '


        <a class="zero-box" href="./41-group-1a"></a>

        <br class="clear">

        <div class="box full">

        ';

        /*$bad = array(".", "..", ".DS_Store", "_notes", "Thumbs.db");

        $linksTop = scandir(dirname(__FILE__).'/../img/links-top');
        $linksTop = array_diff($linksTop, $bad);

        foreach ($linksTop as $key => $path) {
            echo '<img ';
            echo 'class ="project'.implode(' project', explode('-',substr($path,0,5))).'"';
            echo ' src="/'.$localhost.'img/links-top/'.$path.'">';
        }*/


        echo file_get_contents('img/links-top.svg');

        echo '
        </div>

        <br class="clear">

        <a class="zero-box" href="./38-chih-ying-wong"></a>


        ';





/**********************************************************************

    STEP 2

**********************************************************************/

        // echo '<div class="box" style="width:24px"></div>';

        for ($i=0; $i < 6; $i++) {

            if($i+41 == $project[0]) {
                $current = "current";
                $box = "div";
            } else {
                $box = 'a  href="'.($i+41).'-'.toAscii($names[$i+41]).'"';
                $current = "";
            }

            $src = "content/".toAscii($names[$i+41])."/th.jpg";

            if(is_file($src)) $displayName = '<img src="'.substr($src, 0, -4).'.'.filemtime($src).'.jpg">';
            else $displayName = $names[$i+41];

            if($i === 2 || $i === 3) $current .= ' seven ';
            else $current .= ' six ';

            echo '<'.$box.' class="box '.$current.'" '.randomColor($i*6*0-1).'>'.$displayName.'</'.trim(substr($box, 0, 3)).'>';

            echo '

            ';
        }

        echo '


        <a class="zero-box" href="./50-"></a>

        <br class="clear">

        <div class="box full">

        ';

        /*$linksBot = scandir(dirname(__FILE__).'/../img/links-bot');
        $linksBot = array_diff($linksBot, $bad);

        foreach ($linksBot as $key => $path) {
            echo '<img ';
            echo 'class ="project'.implode(' project', explode('-',substr($path,0,5))).'"';
            echo ' src="/'.$localhost.'img/links-bot/'.$path.'">';
        }*/

        echo file_get_contents('img/links-bot.svg');

        echo '
        </div>

        <br class="clear">

        <a class="zero-box" href="./46-groupe-3b"></a>



        ';





/**********************************************************************

    STEP 3

**********************************************************************/

        for ($i=0; $i < 19; $i++) {
            // echo '<div class="box two" '.randomColor($i*2*0-1).'></div>';

            $k = $i + 50;

            if(!isset($names[$k])) {
                $box = "div";
                $current = "";
            }

            else if($k == $project[0]) {
                $current = "current";
                $box = "div";
            } else {
                $box = 'a  href="'.($k).'-'.toAscii($names[$k]).'"';
                $current = "";
            }

            echo '<'.$box.' class="box two '.$current.' ">';
            if(is_dir('content/'.toAscii($names[$k])) && isset($names[$k]))
                echo '<img src="content/'.toAscii($names[$k]).'/th.jpg">';
            echo '</'.trim(substr($box, 0, 3)).'>';

            echo '

            ';

            echo '

            ';
        }


        ?>

        <a class="zero-box" href="./"></a>

        </header>


        <!-- header end -->

