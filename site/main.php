<div class="main clearfix">
	<div class="trio">
		<h2 class="big">1.</h2>

		<p>Students were asked to discover properties of fruits and vegetables</p>

		<p>Actions and photography were the tool used here. A list of actions and flipbook were produced by each student.</p>

		<em>&lt; top row &gt;</em>

	</div>

	<div class="trio">
		<h2 class="big">2.</h2>

		<p>Based on one of the pictures from STEP&nbsp;1, teams of six students were to create a full scale mockup that materialized the properties found in the photograph.</p>

		<p>Physical construction was the focus here. Mockups ranging from ten to a hundred cubic meters were produced in team.</p>

		<em>&lt; middle row &gt;</em>

	</div>

	<div class="trio">
		<h2 class="big">3.</h2>

		<p>Based on the mockups from STEP&nbsp;2, teams of two students were to propose a event box on the Place-des-Festivals that integrated the spatial qualities of the mockup.</p>

		<p>Digital and physical models, rendering, plans and detail section were used to represent the projects. One or both of the proposed sites were investigated.</p>

		<em>&lt; bottom row &gt;</em>

	</div>
</div>