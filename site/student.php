<?php


$name = $names[$project[0]+0];
$url = toAscii($name);

if(!is_null($name) && $name !== "") echo '<h2 class="big center">'.$name.'</h2>';
else echo '<h2 class="big center">[ Name Missing ]</h2>';






/**********************************************************************
    
    STEP 1

**********************************************************************/

if($project[0]+0 < 40 ) {

?>

<div class="media clearfix">

	<?php

		$src = "content/".$project[0]."/photo.jpg";
		if(!file_exists($src))
			$src = "img/no_img.png";
		else
			$src = substr($src, 0, -4).'.'.filemtime($src).'.jpg';
	?>

	<img class="duo" src="<?php echo $src; ?>">

	<?php
		$src = "content/".$project[0]."/drawing.jpg";
		if(!file_exists($src))
			$src = "img/no_img.png";
		else
			// $src = "content/".$project[0]."/drawing.".filemtime($src).".jpg";
			$src = substr($src, 0, -4).'.'.filemtime($src).'.jpg';
	?>

	<img class="duo" src="<?php echo $src; ?>">

	<?php
		$src = "content/".$project[0]."/flipbook.gif";
		if(file_exists($src)) {
			$src = substr($src, 0, -4).'.'.filemtime($src).'.gif';
			echo '<img class="uno" src="'.$src.'">';
		}

		else $src = "content/".$project[0]."/flipbook.m4v";


		if(file_exists($src)) {

			echo '<video class="video" data-width="76em" data-height="42em" controls="controls">
	<source src="'.$src.'" type="video/mp4">
	<object data="'.$src.'" data-width="76em" data-height="42em">
	</object> 
</video>';

		}
			
	?>

</div>

<?php

}






/**********************************************************************
    
    STEP 2

**********************************************************************/

else if( $project[0]+0 < 50 ) {

?>

<div class="step-two clearfix">

	<?php

		/* image integration */
		for ($i = 1; $i <= 5; $i++) { 
			// echo $name;
			$src = "content/".$url."/image".$i.".jpg";
			if(file_exists($src)) {

				$src = "content/".$url."/image".$i.".".filemtime($src).".jpg";
				echo '<img src="'.$src.'">';

			} // else echo $url;
		}

		/* video integration */
		for ($i = 1; $i <= 2; $i++) { 
			// echo $name;
			$src = "content/".$url."/video".$i.".m4v";
			if(file_exists($src)) {

				// $src = "content/".$url."/video".$i.".".filemtime($src).".m4v";
				// echo '<video src="'.$src.'">';

				echo '<video class="video" data-width="76em" data-height="42em" controls="controls">
				  <source src="'.$src.'" type="video/mp4">
				  <object data="'.$src.'" data-width="76em" data-height="42em">
				  </object> 
				</video>';

			} // else echo $url;
		}

		/* vimeo integration */
		for ($i = 1; $i <= 2; $i++) { 
			// echo $name;
			$src = "content/".$url."/vimeo".$i.".txt";
			if(file_exists($src)) {

				// $src = "content/".$url."/image".$i.".".filemtime($src).".txt";

				$adress = file_get_contents($src);

				echo '<iframe class="video" src="http://player.vimeo.com/video/'.$adress.'?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=ffffff" data-width="76em" data-height="43em" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';

			} // else echo $url;
		}

		// $src = "content/".$project[0]."/photo.".filemtime($src).".jpg";

	?>

</div>

<?php

}







/**********************************************************************
    
    STEP 3

**********************************************************************/

else {

?>

<div class="step-three clearfix">


<?php

	$dir = 'content/'.toAscii($names[$project[0]]);

		/* image integration */
		foreach(scandir($dir) as $key => $value) {
			// echo $name;
			if(in_array($value, array(".","..",".DS_Store","th.jpg")))
				continue;

			// echo $value;

			echo "<br><br>";

			$src = $dir . "/" . $value;
			if(file_exists($src)) {

				$src = substr_replace($src, ".".filemtime($src), -4, 0);
				echo '<img src="'.$src.'">';

			} // else echo $url;
		}

?>

</div>

<?php

}

?>

<!-- <br class="clear"> -->