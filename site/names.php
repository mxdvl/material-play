<?php $names = array(

	// STEP 1

	 1 => "Camilla Siggaard Andersen",
	 2 => "Julian Mirabelli",
	 3 => "Stephanie Huss",
	 4 => "Lindsay Townsend",
	 5 => "Yousef Farasat",
	 6 => "Jason Treherne",
	 7 => "Xavier Proulx",
	 8 => "Evelyn Velghe",
	 9 => "Kimberly Boychuk",

	10 => "Madeck",
	11 => "Brian Muthaliff",
	12 => "Robyn Whitwham",
	13 => "Ji Won Jun",
	14 => "Hubert Lemieux",
	15 => "Chloé Blain",
	16 => "Christina Vendittelli",
	17 => "Kelly Anne Caulfield",
	18 => "Nathan Bonneville",
	19 => "Ashleigh Huza",

	20 => "Stefania R Sciascia",
	21 => "Vincent Désy",
	22 => "Vi Ngo",
	23 => "Josiane Crampé",
	24 => "Éloïse Choquette",
	25 => "Eve Lachapelle",
	26 => "Antony Plumb",
	27 => "Veronica Soncini",
	28 => "Kim Landry",
	29 => "Brittany Marshall",

	30 => "Nicki Reckziegel",
	31 => "Joe Shi",
	32 => "Kit Morse",
	33 => "Arouen Soobrayen",
	34 => "Émélie DT",
	35 => "Sara-Jeanne Jacques Dagenais",
	36 => "Alexandre Lapierre",
	37 => "Kelvin Kung",
	38 => "Chih-Ying Wong",

	// STEP 2

	41 => "Group 1A",
	42 => "Group 1B",
	43 => "Group 2A",
	44 => "Group 2B",
	45 => "Group 3A",
	46 => "Group 3B",

	// STEP 3

	50 => "Bonneville - Morse",
	51 => "Caulfield - Sciascia",
	52 => "Choquette - Mirabelli",
	53 => "Dagenais - Blain",
	54 => "Désy - Huss",
	55 => "DT - Marshall",
	56 => "Farasat - Plumb",
	57 => "Huza - Whitwham",
	58 => "Jun - Reckziegel",
	59 => "Kung - Townsend",

	60 => "Lachapelle - Andersen",
	// 61 => "Lapierre - Landry TODO",
	62 => "Lemieux - Treherne",
	63 => "Madeck - Proulx",
	// 64 => "Muthaliff - Soobrayen TODO",
	65 => "Ngo - Soncini",
	66 => "Shi - Vendittelli",
	67 => "Velghe - Boychuk",
	68 => "Wong - Crampé",

	99 => null


);

?>