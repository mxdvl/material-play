<?php

date_default_timezone_set('America/Montreal'); // for mktime() to work

setlocale(LC_ALL, 'en_CA.UTF8');

if(!isset($_GET['stay'])
 && !in_array(substr($_SERVER['HTTP_REFERER'],0,16),
    array('http://localhost', 'http://home.minu'))
 && $_SERVER['SERVER_NAME'] != 'minui34.com')

    header('Location: http://minui34.com'.$_SERVER['REQUEST_URI']);

function toAscii($str, $replace=array(), $delimiter='-') {
	if( !empty($replace) ) {
		$str = str_replace((array)$replace, ' ', $str);
	}

	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
	$clean = strtolower(trim($clean, '-'));
	$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

	return $clean;
}

function randomColor($value = NULL) {
    if(!isset($value)) {

        $color = dechex(floor(rand(0, 256*256*256)));
        return ' style="background-color: #'.$color.';" ';

    } else if($value == -1) {

        return ' ';

    } else {

        $value = max(0, min('360', $value%360));
        $r = max(0, sin(2*PI()*$value/360));
        $g = max(0, sin(2*PI()*($value+120)/360));
        $b = max(0, sin(2*PI()*($value+240)/360));

        if($value < 8) $r = $value / 8; else $r = 1 - ($value-8)/30;
        $g = 1 - min(16, $value) / 16;
        ($value == 0) ? $b = 0.9 : $b = 0.2;

        $a = 8;
        $x = 5;
        $color = dechex(floor($r*$a + $x)).dechex(floor($g*$a + $x)).dechex(floor($b*$a + $x));

        return ' style="background-color: #'.$color.';" ';
    }
}

function cacheBust($path) {
  global $localhost;

  $changeStamp = "." . filemtime($path);

  $start = strrpos($path, ".");

  $newPath = substr_replace($path, $changeStamp, $start, 0);

  return "/".$localhost.$newPath;
}

/* =====================================================
    Handle request
===================================================== */

$request = explode('?', $_SERVER['REQUEST_URI']);
$request = $request[0];

// handle requested uri
$uri = explode('/', $request);
array_shift($uri); // because position 0 is a empty string
if($uri[0] == 'material-play'){
    // deals with localhost testing
    array_shift($uri);
    $localhost = 'material-play/';
} else $localhost = '';

$serveur = $_SERVER['SERVER_NAME'];
$page = $uri[0];
$xhr = isset($_GET['xhr']);

// sets default
if(!isset($page) || $page == '' || $page == 'student') $page = 'main';

// handle project case
$project = explode('-',$page);
if(is_numeric($project[0])) $page = 'student';



/* =====================================================
    Handle Errors
===================================================== */

if(!is_file("site/".$page.".php")) header('Location: error');
if($page=='error') header("404 Not Found");


/* =====================================================
    Send Pages
===================================================== */

require_once "site/names.php";

require_once "site/header.php";


require "site/".$page.".php";


require_once "site/footer.php";

?>