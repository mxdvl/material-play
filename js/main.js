/*
 * 37 = Left
 * 39 = Right
 *
 *
 */

var current = $(".zero-box:first");
var project0 = $('header').attr('id');
if($("div.box.current").length > 0) current = $("div.box.current");

var svgTop, svgBottom;

$(document).ready(function() {
    // svgTop = $('.box.full:first').svg();
    // svgBottom = $('.box.full:last').svg();

    $("._"+project0).attr('stroke-width', 3);
});

$("a.box").hover(function(){
  var project = $(this).attr('href').substr(0,2);

  $("._"+project).toggleClass('hover');
})

/* Callback after loading external document */
function loadDone(svg, error) {
    svg.text(10, 20, error || 'Loaded into ' + this.id);
}

$(document).keydown(function(event) {

	if( event.which == 37 && current.attr("class") != "zero-box") {
		// event.preventDefault();
    if( current.prev('a').length > 0 )
		  window.location = current.prev('a').attr('href');
    else window.location = current.prev().prev().attr('href');
	}

	if( event.which == 39 ) {
		// event.preventDefault();
    if( current.next('a').length > 0 )
		  window.location = current.next('a').attr('href');
    else window.location = current.next().next().attr('href');
	}

});
